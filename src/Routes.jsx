import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import Enterprise from './pages/enterprise';
import PrivateRoute from './components/privateRoute';
import Login from './pages/login';
import Enterprises from './pages/enterprises';

const Routes = () => (
  <Router>
    <Switch>
      <Route exact path="/login">
        <Login />
      </Route>
      <PrivateRoute>
        <Route exact path="/">
          <Enterprises />
        </Route>
        <Route path="/empresas/:slug">
          <Enterprise />
        </Route>
      </PrivateRoute>
    </Switch>
  </Router>
);

export default Routes;
