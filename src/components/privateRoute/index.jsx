import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import { setUnauthenticated } from '../../lib/adapters/redux/auth';
import { UNAUTHORIZED_EVENT } from '../../lib/adapters/api/index';

const PrivateRoute = ({ children, ...rest }) => {
  const isAuthenticated = useSelector(({ auth }) => auth.isAuthenticated);
  const dispatch = useDispatch();
  window.addEventListener(UNAUTHORIZED_EVENT, () => {
    dispatch(setUnauthenticated('Sessão encerrada'));
  });

  return (
    <Route
      {...rest}
      render={() => (
        isAuthenticated ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/login',
            }}
          />
        )
      )}
    />
  );
};

PrivateRoute.propTypes = {
  children: PropTypes.oneOfType([PropTypes.any]).isRequired,
};

export default PrivateRoute;
