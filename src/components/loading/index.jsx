import { useSelector } from 'react-redux';
import { Container, Load } from './styled';

const Loading = () => {
  const isLoading = useSelector(({ loading }) => loading.isLoading);

  return (
    isLoading ? (
      <Container>
        <Load />
      </Container>
    ) : null
  );
};

export default Loading;
