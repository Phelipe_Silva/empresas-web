import styled from 'styled-components';

export const Container = styled.div`
  min-width: 100%;
  min-height: 100%;

  backdrop-filter: blur(1px);
  background-color: rgba(255, 255, 255, 0.6);
  
  position: absolute;
  top: 0;
  left: 0;

  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 1;
`;

export const Load = styled.span`
  display: flex;

  width: 8.25rem;
  height: 8.25rem;
  border-radius: 50%;

  border-color: ${({ theme }) => theme.colors.secondary};
  border-style: solid;
  border-width: 0.6rem;

  border-top-color: transparent;
  animation: spinner infinite 1.2s;

  @keyframes spinner {
    0% {
      transform: rotate(45deg);
    }
    100% {
      transform: rotate(405deg);
    }
  }
`;
