import PropTypes from 'prop-types';
import Layout from './styled';

const Container = ({ children, direction }) => (
  <Layout direction={direction}>{children}</Layout>
);

Container.defaultProps = {
  direction: 'row',
};

Container.propTypes = {
  children: PropTypes.oneOfType([PropTypes.any]).isRequired,
  direction: PropTypes.string,
};

export default Container;
