import styled from 'styled-components';

const Layout = styled.div`
  display: flex;
  flex-direction: ${({ direction }) => direction};

  margin: 0 auto;
  padding: ${({ theme }) => theme.margin.xs};
  max-width: 45rem;
  width: 100%;
`;

export default Layout;
