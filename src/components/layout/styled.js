import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  background-color: ${({ theme }) => theme.colors.background};
  
  * {
    font-family: ${({ theme }) => theme.font.family.regular};
    color: ${({ theme }) => theme.colors.dark};
  }
`;

export default Container;
