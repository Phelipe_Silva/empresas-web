import PropTypes from 'prop-types';
import { Container } from './styled';

const Layout = ({ children }) => (
  <Container>{children}</Container>
);

Layout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default Layout;
