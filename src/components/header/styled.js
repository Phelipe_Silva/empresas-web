import styled from 'styled-components';

export const Container = styled.nav`
  display: flex;
  align-items: center;
  
  margin: 0;
  padding: 1rem;
  height: 9.4rem;

  color: ${({ theme }) => theme.colors.light};
  background-image: ${({ theme }) => (
    `linear-gradient(
      180deg,
      ${theme.colors.primary} 0%,
      ${theme.colors.darken} 360%
    )`
  )};
`;

export default Container;
