import PropTypes from 'prop-types';
import { Container } from './styled';
import RowContainer from '../container';

const Header = ({ children }) => (
  <Container>
    <RowContainer direction="row">
      {children}
    </RowContainer>
  </Container>
);

Header.propTypes = {
  children: PropTypes.oneOfType([PropTypes.any]).isRequired,
};

export default Header;
