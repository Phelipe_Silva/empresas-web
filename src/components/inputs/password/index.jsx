import { useState } from 'react';
import PropTypes from 'prop-types';
import TextInput from '../text';
import { ReactComponent as LockSVG } from '../../../assets/icons/ic-cadeado.svg';
import { ReactComponent as EyeOutlineSVG } from '../../../assets/icons/ic-eye-outline.svg';
import { ReactComponent as EyeFilledSVG } from '../../../assets/icons/ic-eye-filled.svg';

const PasswordInput = ({
  placeholder,
  value,
  onChange,
  ...props
}) => {
  const [showPassword, setShowPasswod] = useState(false);
  return (
    <TextInput
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      autoComplete="current-password"
      prependIcon={<LockSVG />}
      type={(
        showPassword ? 'text' : 'password'
      )}
      appendIcon={(
        showPassword ? <EyeOutlineSVG /> : <EyeFilledSVG />
      )}
      onClickAppend={() => setShowPasswod((show) => !show)}
      {...props}
    />
  );
};

PasswordInput.defaultProps = {
  placeholder: 'Senha',
  value: '',
};

PasswordInput.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

export default PasswordInput;
