import React from 'react';
import { ThemeProvider } from 'styled-components';
import 'jest-styled-components';
import { render } from '@testing-library/react';
import PasswordInput from './index';
import { theme } from '../../theme';

it('renders correctly', () => {
  const handler = jest.fn();
  const { asFragment } = render(
    <ThemeProvider
      theme={theme}
    >
      <PasswordInput onChange={handler} />
    </ThemeProvider>,
  );
  expect(asFragment(
    <PasswordInput onChange={handler} />,
  )).toMatchSnapshot();
});
