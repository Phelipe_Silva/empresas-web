import PropTypes from 'prop-types';
import Button from '../../button';
import {
  Container,
  Input,
  Icon,
  ErrorTip,
} from './styled';

const TextInput = ({
  prependIcon,
  primary,
  appendIcon,
  onClickAppend,
  error,
  ...props
}) => (
  <Container primary={primary} data-error={error}>
    <Icon>{prependIcon}</Icon>
    <Input {...props} />
    {(error || appendIcon) && (
      <Button type="button" icon>
        { error ? (
          <ErrorTip onClick={onClickAppend}>!</ErrorTip>
        ) : (
          <Icon onClick={onClickAppend}>
            {appendIcon}
          </Icon>
        )}
      </Button>
    )}
  </Container>
);

TextInput.defaultProps = {
  prependIcon: null,
  appendIcon: null,
  onClickAppend: null,
  primary: false,
  ref: null,
  error: false,
};

TextInput.propTypes = {
  prependIcon: PropTypes.element,
  appendIcon: PropTypes.element,
  onClickAppend: PropTypes.func,
  primary: PropTypes.bool,
  error: PropTypes.bool,
  ref: PropTypes.shape({ current: PropTypes.instanceOf(PropTypes.element) }),
};

export default TextInput;
