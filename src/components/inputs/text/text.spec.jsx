import React from 'react';
import { ThemeProvider } from 'styled-components';
import 'jest-styled-components';
import { render } from '@testing-library/react';
import TextInput from './index';
import { theme } from '../../theme';

it('renders correctly', () => {
  const { asFragment } = render(<ThemeProvider theme={theme}><TextInput /></ThemeProvider>);
  expect(asFragment(<TextInput />)).toMatchSnapshot();
});
