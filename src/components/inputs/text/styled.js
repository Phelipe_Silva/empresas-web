import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  
  padding: ${({ theme }) => theme.padding.xs};
  margin: .6rem 0;
  
  border-bottom-style: solid;
  border-bottom: ${({ theme }) => theme.border.xs} solid;
  border-color: ${({ theme, primary }) => (
    primary ? theme.colors.light : theme.colors.dark
  )};
  position: relative;

  &[data-error='true'] {
    border-color: ${({ theme }) => theme.colors.error};
  }

  > input::placeholder {
    color: ${({ theme, primary }) => (primary ? theme.colors.primaryDarken : theme.colors.dark)};
  }
  > input {
    color: ${({ theme, primary }) => (primary ? theme.colors.light : theme.colors.dark)};
  }
`;

export const Input = styled.input`
  padding: ${({ theme }) => theme.padding.xs};
  border: none;
  font-size: ${({ theme }) => theme.font.size.body};
  flex-grow: 1;
  background-color: transparent;

  @media(max-width: 700px) {
    font-size: ${({ theme }) => theme.font.size.paragraph};
  }
`;

export const Icon = styled.span`
  padding: ${({ theme }) => theme.padding.xs};

  & svg{
    height: 2rem;
    width: 2rem;
  }
  @media(max-width: 700px) {
    & svg{
      width: 1.5rem;
      height: 1.5rem;
    }
  }
`;

export const AppendIcon = styled.span`
  padding: ${({ theme }) => theme.padding.xs};
`;

export const ErrorTip = styled.span`
  text-align: center;
  line-height: 20px;
  display: block;
  background-color: ${({ theme }) => theme.colors.error};
  color: ${({ theme }) => theme.colors.light};
  width: 20px;
  height: 20px;
  border-radius: 50%;
`;
