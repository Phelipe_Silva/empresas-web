import PropTypes from 'prop-types';
import TextInput from '../text';
import { ReactComponent as EmailSVG } from '../../../assets/icons/ic-email.svg';

const EmailInput = ({
  placeholder,
  value,
  onChange,
  ...props
}) => (
  <TextInput
    placeholder={placeholder}
    type="email"
    autoComplete="email"
    value={value}
    onChange={onChange}
    prependIcon={<EmailSVG />}
    {...props}
  />
);

EmailInput.defaultProps = {
  placeholder: 'E-mail',
  value: '',
};

EmailInput.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

export default EmailInput;
