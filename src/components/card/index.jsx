import PropTypes from 'prop-types';
import { Container } from './styled';

const Card = ({ children, width }) => (
  <Container width={width}>
    {children}
  </Container>
);

Card.defaultProps = {
  width: null,
};

Card.propTypes = {
  width: PropTypes.oneOfType([PropTypes.string]),
  children: PropTypes.oneOfType([PropTypes.any]).isRequired,
};

export default Card;
