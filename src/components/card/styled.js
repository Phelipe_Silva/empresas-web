import styled from 'styled-components';

export const Container = styled.div`
  background-color: ${({ theme }) => theme.colors.light};

  padding: ${({ theme }) => theme.padding.xl};
  margin: ${({ theme }) => theme.margin.xs} 0;
  width: ${({ width }) => width};
  border-radius: 5px;
`;

export default Container;
