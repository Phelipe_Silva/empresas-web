import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';

export const theme = {
  colors: {
    primary: '#ee4c77',
    primaryDarken: '#991237',
    secondary: '#57bbbc',
    background: '#eeecdb',
    dark: '#383743',
    darken: '#0d0430',
    light: '#ffffff',
    error: '#ff0f44',
    grey: '#8d8c8c',
    indigo: '#1a0e49',
  },
  padding: {
    xs: '.18rem',
    sm: '.5rem',
    lg: '1rem',
    xl: '1.6rem',
  },
  margin: {
    xss: '0.25rem',
    xs: '1rem',
    sm: '2rem',
  },
  border: {
    xs: '0.063rem',
  },
  font: {
    family: {
      regular: 'Roboto, sans-serif',
    },
    size: {
      title: '1.6rem',
      subheading: '1.5rem',
      body: '1.36rem',
      paragraph: '1.125rem',
      caption: '.76rem',
    },
  },
  container: '500px',
};

const Theme = ({ children }) => (
  <ThemeProvider theme={theme}>
    {children}
  </ThemeProvider>
);

Theme.propTypes = {
  children: PropTypes.element.isRequired,
};

export default Theme;
