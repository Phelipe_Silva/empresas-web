import styled from 'styled-components';

export const Container = styled.button`
  padding: ${({ theme }) => theme.padding.xs};
  height: 100%;
  min-height: ${({ icon }) => !icon && '3rem'};
  
  border: none;
  border-radius: 4px;
  
  width: ${({ width }) => width};
  margin: ${({ icon }) => !icon && '0.6rem'};
  
  letter-spacing: 0.04px;
  font-size: ${({ theme }) => theme.font.size.paragraph};
  text-transform: uppercase;
  font-weight: bold;
  
  background-color: ${({ theme, icon }) => (icon ? 'transparent' : theme.colors.secondary)};
  color: ${({ theme }) => theme.colors.light};

  &:disabled{
    background-color: ${({ theme }) => theme.colors.grey};
  }
  &:not(:disabled) {
    cursor: pointer;
  }
`;

export default Container;
