import PropTypes from 'prop-types';
import { Container } from './styled';

const Button = ({ children, icon, ...props }) => (
  <Container icon={icon} {...props}>{children}</Container>
);

Button.defaultProps = {
  type: 'submit',
  icon: false,
};

Button.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
  ]).isRequired,
  icon: PropTypes.bool,
  type: PropTypes.string,
};

export default Button;
