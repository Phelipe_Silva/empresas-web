import { Provider } from 'react-redux';
import Theme from './components/theme';
import Layout from './components/layout';
import store from './lib/adapters/redux';
import Loading from './components/loading';
import Routes from './Routes';

function App() {
  return (
    <Theme>
      <Layout>
        <Provider store={store}>
          <Loading />
          <Routes />
        </Provider>
      </Layout>
    </Theme>
  );
}

export default App;
