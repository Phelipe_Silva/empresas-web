import Header from '../../components/header';
import EntrepriseList from './components/enterpriseList';
import EnterpriseFilter from './components/enterpriseFilter';

const Enterprises = () => (
  <>
    <Header>
      <EnterpriseFilter />
    </Header>
    <EntrepriseList />
  </>
);

export default Enterprises;
