import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import TextInput from '../../../../components/inputs/text';
import { filterEnterprises } from '../../../../lib/adapters/redux/enterprises';
import { Logo, LogoContainer, Button } from './styled';
import { ReactComponent as SearchSVG } from '../../../../assets/icons/ic-search.svg';
import { ReactComponent as CloseSVG } from '../../../../assets/icons/ic-close.svg';
import LogoImg from '../../../../assets/logo-nav.png';
import debounce from '../../../../lib/adapters/utils/debounce';

const EnterpriseFilter = () => {
  const dispatch = useDispatch();
  const [search, setSearch] = useState('');
  const [showSearch, setShowSearch] = useState(false);
  const isLoading = useSelector(({ loading }) => loading.isLoading);

  debounce(search, () => {
    dispatch(filterEnterprises(search));
  });

  return (
    <>
      { showSearch ? (
        <TextInput
          primary
          autoFocus
          placeholder="Pesquisar"
          disabled={isLoading}
          prependIcon={<SearchSVG />}
          appendIcon={<CloseSVG />}
          onInput={({ target }) => setSearch(target.value)}
          onClickAppend={() => setShowSearch(false)}
        />
      ) : (
        <>
          <LogoContainer>
            <Logo src={LogoImg} />
          </LogoContainer>
          <Button
            title="Buscar empresas"
            icon
            onClick={() => setShowSearch(true)}
          >
            <SearchSVG />
          </Button>
        </>
      ) }
    </>
  );
};

export default EnterpriseFilter;
