import styled from 'styled-components';
import BaseButton from '../../../../components/button';

export const LogoContainer = styled.span`
  display: flex;
  flex-grow: 1;
  align-items: center;
  justify-content: center;
`;

export const Logo = styled.img`
  max-width: 100%;

  @media(max-width: 700px) {
    max-height: 2.5rem;
  }
`;

export const Icon = styled.span`
  width: 2rem;
  height: 2rem;
`;

export const Button = styled(BaseButton)`
  & svg{
    height: 2.5rem;
    width: 2.5rem;
  }

  @media(max-width: 700px) {
    & svg{
      width: 2rem;
      height: 2rem;
    }
  }
`;
