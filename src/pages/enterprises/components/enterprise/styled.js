import styled from 'styled-components';
import { Link as DOMLink } from 'react-router-dom';

export const Layout = styled.div`
  display: grid;
  grid-template-columns: 1fr 2fr;
  grid-gap: ${({ theme }) => theme.margin.sm};

  @media (max-width: 720px) {
    grid-template-columns: 1fr;
  }
`;

export const Image = styled.img`
  object-fit: contain;
  object-position: center;
  width: 100%;
`;

export const Title = styled.h2`
  font-size: ${({ theme }) => theme.font.size.subheading};
  font-weight: bold;
  color: ${({ theme }) => theme.colors.indigo};
`;
export const Type = styled.h3`
  font-size: ${({ theme }) => theme.font.size.body};
  font-weight: normal;
  color: ${({ theme }) => theme.colors.grey};
`;

export const Country = styled.span`
  font-size: ${({ theme }) => theme.font.size.caption};
  color: ${({ theme }) => theme.colors.grey};
`;

export const Details = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const Link = styled(DOMLink)`
  text-decoration: none;
`;
