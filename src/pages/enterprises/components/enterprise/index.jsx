import PropTypes from 'prop-types';
import { useLocation } from 'react-router-dom';
import Card from '../../../../components/card';
import useAsset from '../../../../lib/adapters/utils/asset';
import useSlugify from '../../../../lib/adapters/utils/slugify';
import {
  Layout,
  Image,
  Details,
  Title,
  Type,
  Country,
  Link,
} from './styled';

const Enterprise = ({ enterprise }) => {
  const location = useLocation();
  const { assetUrl } = useAsset();
  const { slugify } = useSlugify();

  return (
    <Card key={enterprise.id}>
      {location.state}
      <Layout>
        <Image
          alt={`Foto da empresa ${enterprise.enterprise_name}`}
          title={`Foto da empresa ${enterprise.enterprise_name}`}
          src={assetUrl(enterprise.photo)}
        />
        <Details>
          <Link
            title={`Visualizar detalhes da empresa ${enterprise.enterprise_name}`}
            to={{
              pathname: `/empresas/${slugify(enterprise.enterprise_name)}`,
              state: { id: enterprise.id },
            }}
          >
            <Title>{enterprise.enterprise_name}</Title>
          </Link>
          <Type>{enterprise.enterprise_type.enterprise_type_name}</Type>
          <Country>{enterprise.country}</Country>
        </Details>
      </Layout>
    </Card>
  );
};

Enterprise.propTypes = {
  enterprise: PropTypes.oneOfType([PropTypes.object]).isRequired,
};

export default Enterprise;
