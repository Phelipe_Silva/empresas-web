import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Message } from './styled';
import Container from '../../../../components/container';
import Enterprise from '../enterprise';

const EnterpriseList = () => {
  const [message, setMessage] = useState();
  const [allEnterprises, error] = useSelector(({ enterprises }) => (
    [enterprises.all, enterprises.error]
  ));

  useEffect(() => {
    if (error) {
      setMessage(error);
    } else if (!error && !allEnterprises.length) {
      setMessage('Clique na busca para iniciar.');
    } else {
      setMessage(null);
    }
  }, [error, allEnterprises.length]);

  return (
    <Container direction="column">
      { message ? (
        <Message>{message}</Message>
      ) : (
        allEnterprises.map((enterprise) => (
          <Enterprise
            key={enterprise.id}
            enterprise={enterprise}
          />
        ))
      ) }
    </Container>
  );
};

export default EnterpriseList;
