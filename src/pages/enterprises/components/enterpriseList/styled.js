import styled from 'styled-components';

export const Message = styled.p`
  font-size: ${({ theme }) => theme.font.size.paragraph};
  color: ${({ theme }) => theme.colors.grey};
  text-align: center;
  margin-top: 25vh;
`;

export default Message;
