import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import Card from '../../components/card';
import Container from '../../components/container';
import Header from '../../components/header';
import { getEnterprise, setEnterprise } from '../../lib/adapters/redux/enterprise';
import useAsset from '../../lib/adapters/utils/asset';
import { ReactComponent as ArrowLeft } from '../../assets/icons/ic-arrow-left.svg';
import {
  Button,
  Description,
  Image,
  Title,
} from './styled';

const Enterprise = () => {
  const { assetUrl } = useAsset();
  const dispatch = useDispatch();
  const location = useLocation();
  const history = useHistory();
  const enterprise = useSelector(({ enterprise: data }) => (
    data.enterprise
  ));
  useEffect(() => {
    dispatch(getEnterprise(location.state.id));
    return dispatch(setEnterprise(null));
  }, []);
  return (
    <>
      <Header>
        <Button icon onClick={() => history.goBack()}>
          <ArrowLeft />
        </Button>
        <Title>
          {enterprise ? enterprise.enterprise_name : ''}
        </Title>
      </Header>
      { enterprise && (
        <Container>
          <Card width="100%">
            <Image
              alt={`Foto da empresa ${enterprise.enterprise_name}`}
              title={`Foto da empresa ${enterprise.enterprise_name}`}
              src={assetUrl(enterprise.photo)}
            />
            <Description>{enterprise.description}</Description>
          </Card>
        </Container>
      )}
    </>
  );
};

export default Enterprise;
