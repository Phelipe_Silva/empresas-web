import styled from 'styled-components';
import BaseButton from '../../components/button';

export const Image = styled.img`
  object-fit: contain;
  object-position: center;
  width: 100%;
`;

export const Description = styled.p`
  margin: ${({ theme }) => theme.margin.sm} 0;
  font-size: ${({ theme }) => theme.font.size.paragraph};
  color: ${({ theme }) => theme.colors.grey};
`;
export const Title = styled.h1`
  display: flex;
  align-items: center;
  font-size: ${({ theme }) => theme.font.size.title};
  color: ${({ theme }) => theme.colors.light};
  text-transform: uppercase;

@media(max-width: 700px) {
  font-size: ${({ theme }) => theme.font.size.body};
}
`;

export const Button = styled(BaseButton)`
  & svg{
    height: 2.5rem;
    width: 2.5rem;
  }
  @media(max-width: 700px) {
    & svg {
      width: 1.5rem;
      height: 1.5rem;
    }
  }
`;
