import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import EmailInput from '../../components/inputs/email';
import PasswordInput from '../../components/inputs/password';
import Button from '../../components/button';
import LogoImg from '../../assets/logo-home@3x.png';
import { signIn } from '../../lib/adapters/redux/auth';
import {
  Container,
  Description,
  Logo,
  Welcome,
  Form,
  Errors,
} from './styled';

const Login = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const isLoading = useSelector(({ loading }) => loading.isLoading);
  const [isAuthenticated, error] = useSelector(({ auth }) => (
    [auth.isAuthenticated, auth.error]
  ));
  const [state, setState] = useState({
    email: '',
    password: '',
  });

  const handle = (property, { target }) => {
    setState((prev) => ({
      ...prev,
      [property]: target.value,
    }));
  };

  useEffect(() => {
    if (isAuthenticated) {
      history.replace('/');
    }
  }, [isAuthenticated]);

  const onSubmit = async (e) => {
    e.preventDefault();
    dispatch(signIn(state));
  };

  return (
    <Container>
      <Logo src={LogoImg} />
      <Welcome>Bem-Vindo ao Empresas</Welcome>
      <Description>
        Este é um projeto desenvolvido durante o processo seletivo da ioasys
      </Description>
      <Form onSubmit={onSubmit}>
        <EmailInput
          error={!!error}
          title="E-mail"
          required
          value={state.email}
          onChange={(e) => handle('email', e)}
        />
        <PasswordInput
          error={!!error}
          title="Senha"
          required
          value={state.password}
          onChange={(e) => handle('password', e)}
        />
        { error && <Errors>{error}</Errors> }
        <Button
          disabled={isLoading}
        >
          Entrar
        </Button>
      </Form>
    </Container>
  );
};

export default Login;
