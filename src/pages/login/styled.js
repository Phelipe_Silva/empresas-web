import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-grow: 1;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: ${({ theme }) => theme.colors.background};
  max-width: 20rem;

  margin: 0 auto;
`;

export const Welcome = styled.h1`
  font-size: ${({ theme }) => theme.font.size.title};
  text-transform: uppercase;
  letter-spacing: -1.2px;
  max-width: 15rem;
  font-weight: bold;
  text-align: center;
`;

export const Description = styled.p`
  font-size: ${({ theme }) => theme.font.size.paragraph};
  line-height: 1.48;
  text-align: center;

  margin: 1.28rem 0;
`;

export const Logo = styled.img`
  width: 18.438rem;
  height: 4.5rem;
  object-fit: contain;

  margin-bottom: 4rem;
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

export const Errors = styled.span`
  font-size: ${({ theme }) => theme.font.size.caption};
  color: ${({ theme }) => theme.colors.error};
  text-align: center;

  margin: ${({ theme }) => theme.margin.xss} 0;
`;
