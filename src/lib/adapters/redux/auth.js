import { createSlice } from '@reduxjs/toolkit';
import SignInApiService from '../../services/signInApiService';
import UserSignCase from '../../useCases/signIn';
import useApi from '../api';
import { setLoading } from './loading';

const {
  api,
  getStoredCredentials,
  setCredentials,
  hasAuthentication,
} = useApi();

export const auth = createSlice({
  name: 'auth',
  initialState: {
    credentials: getStoredCredentials(),
    isAuthenticated: hasAuthentication(),
    error: null,
  },
  reducers: {
    setAuthenticated(state, { payload }) {
      state.error = null;
      state.credentials = payload;
      state.isAuthenticated = true;
    },
    setUnauthenticated(state, { payload }) {
      state.credentials = null;
      state.error = payload;
      state.isAuthenticated = false;
    },
    setError(state, { payload }) {
      state.error = payload;
    },
  },
});

export const { setAuthenticated, setUnauthenticated, setError } = auth.actions;

export const signIn = (userCredentials) => async (dispatch) => {
  const signInApi = new SignInApiService(api);
  const userSignIn = new UserSignCase(signInApi);
  dispatch(setLoading(true));
  const { isSuccess, data, message } = await userSignIn.signIn(userCredentials);
  dispatch(setLoading(false));
  if (isSuccess) {
    dispatch(setAuthenticated(data));
    setCredentials(data);
  } else {
    dispatch(setError(message));
  }
};

export default auth.reducer;
