import { configureStore } from '@reduxjs/toolkit';
import authReducer from './auth';
import loadingReducer from './loading';
import enterprisesReducer from './enterprises';
import enterpriseReducer from './enterprise';

export default configureStore({
  reducer: {
    auth: authReducer,
    loading: loadingReducer,
    enterprises: enterprisesReducer,
    enterprise: enterpriseReducer,
  },
});
