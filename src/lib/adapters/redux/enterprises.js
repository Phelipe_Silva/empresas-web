import { createSlice } from '@reduxjs/toolkit';
import EnterpriseApiService from '../../services/enterpriseApiService';
import EnterpriseIndexCase from '../../useCases/enterpriseIndex';
import EnterpriseShowCase from '../../useCases/enterpriseShow';
import useApi from '../api';
import { setLoading } from './loading';

export const enterprises = createSlice({
  name: 'enterprises',
  initialState: {
    all: [],
    enterprise: null,
    error: null,
  },
  reducers: {
    setEnterprises: (state, { payload }) => {
      state.all = payload;
      state.error = null;
    },
    setEnterprise: (state, { payload }) => {
      state.enterprise = payload;
    },
    setError: (state, { payload }) => {
      state.error = payload;
    },
  },
});

export const { setEnterprises, setEnterprise, setError } = enterprises.actions;

export const indexEnterprises = () => async (dispatch) => {
  const { api } = useApi();
  dispatch(setLoading(true));
  const enterpriseApi = new EnterpriseApiService(api);
  const enterpriseIndex = new EnterpriseIndexCase(enterpriseApi);
  const { isSuccess, data } = await enterpriseIndex.all();
  dispatch(setLoading(false));
  if (isSuccess) {
    dispatch(setEnterprises(data));
  }
};

export const filterEnterprises = (name) => async (dispatch) => {
  const { api } = useApi();
  dispatch(setLoading(true));
  const enterpriseApi = new EnterpriseApiService(api);
  const enterpriseIndex = new EnterpriseIndexCase(enterpriseApi);
  const { isSuccess, data, message } = await enterpriseIndex.filter(name);
  dispatch(setLoading(false));
  if (isSuccess) {
    dispatch(setEnterprises(data));
  } else {
    dispatch(setError(message));
  }
};

export const getEnterprise = (id) => async (dispatch) => {
  const { api } = useApi();
  dispatch(setLoading(true));
  const enterpriseApi = new EnterpriseApiService(api);
  const enterpriseShow = new EnterpriseShowCase(enterpriseApi);
  const { isSuccess, data } = await enterpriseShow.show(id);
  dispatch(setLoading(false));
  if (isSuccess) {
    dispatch(setEnterprise(data));
  } else {
    setError(data.message);
  }
};

export default enterprises.reducer;
