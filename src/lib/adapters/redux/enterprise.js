import { createSlice } from '@reduxjs/toolkit';
import EnterpriseApiService from '../../services/enterpriseApiService';
import EnterpriseShowCase from '../../useCases/enterpriseShow';
import useApi from '../api';
import { setLoading } from './loading';

export const enterprise = createSlice({
  name: 'enterprise',
  initialState: {
    all: [],
    enterprise: null,
    error: null,
  },
  reducers: {
    setEnterprise: (state, { payload }) => {
      state.enterprise = payload;
    },
    setError: (state, { payload }) => {
      state.error = payload;
    },
  },
});

export const { setEnterprise, setError } = enterprise.actions;

export const getEnterprise = (id) => async (dispatch) => {
  const { api } = useApi();
  dispatch(setLoading(true));
  const enterpriseApi = new EnterpriseApiService(api);
  const enterpriseShow = new EnterpriseShowCase(enterpriseApi);
  const { isSuccess, data } = await enterpriseShow.show(id);
  dispatch(setLoading(false));
  if (isSuccess) {
    dispatch(setEnterprise(data));
  } else {
    setError(data.message);
  }
};

export default enterprise.reducer;
