import { createSlice } from '@reduxjs/toolkit';

export const loading = createSlice({
  name: 'loading',
  initialState: {
    isLoading: false,
  },
  reducers: {
    setIsLoading: (state, { payload }) => {
      state.isLoading = payload;
    },
  },
});

export const { setIsLoading } = loading.actions;

export const setLoading = (loadingState) => (dispatch) => {
  const timeout = loadingState ? 100 : 300;
  setTimeout(() => {
    dispatch(setIsLoading(loadingState));
  }, timeout);
};

export default loading.reducer;
