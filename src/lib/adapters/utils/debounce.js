import { useEffect } from 'react';

const useDebounce = (input, handler, wait = 800) => {
  useEffect(() => {
    if (input) {
      const timerId = setTimeout(handler, wait);
      return () => clearTimeout(timerId);
    }
    return false;
  }, [input, wait]);
};

export default useDebounce;
