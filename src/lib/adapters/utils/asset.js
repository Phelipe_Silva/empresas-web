const useAsset = () => {
  const url = process.env.REACT_APP_API_URL;

  const assetUrl = (path) => `${url}/${path}`;

  return { assetUrl };
};

export default useAsset;
