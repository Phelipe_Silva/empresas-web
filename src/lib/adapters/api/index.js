import axios from 'axios';

export const UNAUTHORIZED_EVENT = 'UNAUTHORIZED';
const CREDENTIALS_KEY = 'credentials';
const HTTP_UNAUTHORIZED_CODE = 401;

const storeCredentials = (data) => {
  const credentials = data ? {
    'access-token': data.accessToken,
    client: data.client,
    uid: data.uid,
  } : null;
  localStorage.setItem(CREDENTIALS_KEY, JSON.stringify(credentials));
};

const getStoredCredentials = () => {
  try {
    const stored = localStorage.getItem(CREDENTIALS_KEY);
    return stored ? JSON.parse(stored) : false;
  } catch (e) {
    storeCredentials(null);
    return false;
  }
};

const useApi = (buss = window) => {
  const api = axios.create({
    baseURL: process.env.REACT_APP_API_URL,
    headers: getStoredCredentials() || {},
  });

  const setCredentials = (credentials) => {
    api.defaults.headers = credentials;
    storeCredentials(credentials);
  };

  api.interceptors.response.use(
    (response) => response,
    ({ response }) => {
      if (response.status === HTTP_UNAUTHORIZED_CODE) {
        storeCredentials(null);
        if (buss) {
          const event = new Event(UNAUTHORIZED_EVENT);
          buss.dispatchEvent(event);
        }
      }
    },
  );

  const hasAuthentication = () => (
    ['access-token', 'client', 'uid'].every((key) => (
      Object.keys(api.defaults.headers).includes(key)
    ))
  );

  return {
    api,
    setCredentials,
    hasAuthentication,
    getStoredCredentials,
  };
};

export default useApi;
