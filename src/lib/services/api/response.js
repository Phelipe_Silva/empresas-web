export const successResponse = (data, message = 'Sucesso') => ({
  message,
  data,
  isSuccess: true,
});

export const errorResponse = (message, data = {}) => ({
  message,
  data,
  isSuccess: false,
});

const build = (data, isSuccess) => {
  switch (Boolean(isSuccess)) {
    case true: return successResponse(data);
    case false: return errorResponse();
    default: throw new Error('Invalid response');
  }
};

export default build;
