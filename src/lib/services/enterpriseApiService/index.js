import { successResponse, errorResponse } from '../api/response';

class EnterpriseService {
  constructor(api) {
    this.api = api;
  }

  async all() {
    try {
      const response = await this.api.get(
        '/api/v1/enterprises',
      );
      const { data } = response;
      return successResponse(data.enterprises);
    } catch (e) {
      return errorResponse('Falha ao listar empressas');
    }
  }

  async filter(name) {
    try {
      const response = await this.api.get(
        '/api/v1/enterprises',
        {
          params: {
            name,
          },
        },
      );
      const { data } = response;
      if (data.enterprises && data.enterprises.length) {
        return successResponse(data.enterprises);
      }
      return errorResponse('Nenhuma empresa foi encontrada para a busca realizada.');
    } catch (e) {
      return errorResponse('Falha ao buscar empressas');
    }
  }

  async show(id) {
    try {
      const response = await this.api.get(
        `/api/v1/enterprises/${id}`,
      );
      const { data } = response;
      return successResponse(data.enterprise);
    } catch (e) {
      return errorResponse('Falha ao buscar empresa');
    }
  }
}

export default EnterpriseService;
