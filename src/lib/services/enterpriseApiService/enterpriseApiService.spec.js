import EnterpriseService from '.';

describe('EnterpriseService', () => {
  let api;
  let enterprises;

  beforeEach(() => {
    enterprises = [
      { id: 5, enterprise_name: 'test', city: 'BH' },
      { id: 6, enterprise_name: 'test2', city: 'SP' },
    ];
  });

  test('should index all enterprises', async () => {
    api = {
      get: jest.fn().mockReturnValue(Promise.resolve({
        data: {
          enterprises,
          success: true,
        },
      })),
    };

    const enterpriseService = new EnterpriseService(api);
    const response = await enterpriseService.all();

    expect(api.get).toBeCalledWith('/api/v1/enterprises');
    expect(response).toStrictEqual({
      isSuccess: true,
      message: 'Sucesso',
      data: enterprises,
    });
  });

  test('should index all enterprises', async () => {
    api = {
      get: jest.fn().mockReturnValue(Promise.resolve({
        data: {
          enterprises,
          success: true,
        },
      })),
    };

    const enterpriseService = new EnterpriseService(api);
    const response = await enterpriseService.filter('test2');

    expect(api.get).toBeCalledWith('/api/v1/enterprises', {
      params: {
        name: 'test2',
      },
    });
    expect(response).toStrictEqual({
      isSuccess: true,
      message: 'Sucesso',
      data: enterprises,
    });
  });
});
