import { successResponse, errorResponse } from '../api/response';

export default class SignInApiService {
  constructor(api) {
    this.api = api;
  }

  async signInWithCredential(credential) {
    try {
      const response = await this.api.post(
        '/api/v1/users/auth/sign_in',
        credential,
      );

      const { data, headers } = response;
      if (data.success) {
        const responseData = {
          accessToken: headers['access-token'],
          client: headers.client,
          uid: headers.uid,
          data: {
            investor: data.investor,
            enterprise: data.enterprise,
          },
        };
        return successResponse(responseData);
      }
      return errorResponse('Erro desconhecido, tente novamente mais tarde.');
    } catch (e) {
      return errorResponse('Credenciais informadas são invalidas, tente novamente.');
    }
  }
}
