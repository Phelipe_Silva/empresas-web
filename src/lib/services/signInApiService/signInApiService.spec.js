import SignInApiService from '.';

describe('signInApiService', () => {
  let api;

  test('should call auth sign in with credentials', async () => {
    const headers = {
      client: 'Jj7jQSICcLsytP4ok6fmRu',
      uid: 'testeapple@ioasys.com.br',
      'access-token': '1qAdHhNzwtD3z3fptGLryw',
    };
    const authentication = {
      investor: { id: 1 },
      enterprise: {},
    };

    api = {
      post: jest.fn().mockReturnValue(Promise.resolve({
        headers,
        data: {
          ...authentication,
          success: true,
        },
      })),
    };

    const signInApiService = new SignInApiService(api);
    const credential = { emaila: 'user@email.com', password: 'password' };
    const response = await signInApiService.signInWithCredential(credential);

    expect(api.post).toBeCalledWith('/api/v1/users/auth/sign_in', credential);
    expect(response).toStrictEqual({
      message: 'Sucesso',
      isSuccess: true,
      data: {
        client: headers.client,
        accessToken: headers['access-token'],
        uid: headers.uid,
        data: authentication,
      },
    });
  });
});
