class EnterpriseShow {
  constructor(enterpriseService) {
    this.enterpriseService = enterpriseService;
  }

  show(id) {
    return this.enterpriseService.show(id);
  }
}

export default EnterpriseShow;
