import EnterpriseIndexCase from '.';

describe('EnterpriseIndexCase', () => {
  let enterpriseIndex;
  let enterprises;

  beforeEach(() => {
    enterprises = [
      { id: 5, enterprise_name: 'test', city: 'BH' },
      { id: 6, enterprise_name: 'test2', city: 'SP' },
    ];
  });

  test('should index all enterprises of service', async () => {
    enterpriseIndex = {
      all: jest.fn().mockReturnValue(Promise.resolve(enterprises)),
    };

    const enterpriseIndexCase = new EnterpriseIndexCase(enterpriseIndex);
    const response = await enterpriseIndexCase.all();

    expect(enterpriseIndex.all).toBeCalled();
    expect(response).toBe(enterprises);
  });

  test('should filter enterprises of service', async () => {
    enterpriseIndex = {
      filter: jest.fn().mockReturnValue(Promise.resolve(enterprises)),
    };

    const enterpriseIndexCase = new EnterpriseIndexCase(enterpriseIndex);
    const response = await enterpriseIndexCase.filter('test');

    expect(enterpriseIndex.filter).toBeCalled();
    expect(response).toBe(enterprises);
  });
});
