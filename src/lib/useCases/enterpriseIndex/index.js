class EnterpriseIndexCase {
  constructor(enterpriseService) {
    this.enterpriseService = enterpriseService;
  }

  all() {
    return this.enterpriseService.all();
  }

  filter(name) {
    return this.enterpriseService.filter(name);
  }
}

export default EnterpriseIndexCase;
