import UserSignCase from '.';

describe('UserSignCase', () => {
  let signService;

  test('should sign in with credential', async () => {
    const authentication = { client: '45621sdf-f25', uuid: 'user@email.com' };
    signService = {
      signInWithCredential: jest.fn()
        .mockReturnValue(Promise.resolve(authentication)),
    };

    const userSigncase = new UserSignCase(signService);
    const credential = { email: 'user@email.com', password: 'password' };
    const response = await userSigncase.signIn(credential);

    expect(signService.signInWithCredential).toBeCalledWith(credential);
    expect(response).toBe(authentication);
  });
});
