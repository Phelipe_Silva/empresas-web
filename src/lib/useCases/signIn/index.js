class UserSignCase {
  constructor(signService) {
    this.signService = signService;
  }

  signIn(credential) {
    return this.signService.signInWithCredential(credential);
  }
}

export default UserSignCase;
